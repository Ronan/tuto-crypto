package com.leroy.ronan.crypto;

import com.leroy.ronan.crypto.asymmetric.*;
import com.leroy.ronan.crypto.mixed.MixedDecrypter;
import com.leroy.ronan.crypto.mixed.MixedEncrypter;
import com.leroy.ronan.crypto.symmetric.SymmetricDecrypter;
import com.leroy.ronan.crypto.symmetric.SymmetricEncrypter;
import com.leroy.ronan.crypto.symmetric.SymmetricKey;
import com.leroy.ronan.crypto.symmetric.SymmetricKeysGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class CryptoShould {

    @Test
    public void encrypt_and_decrypt_a_message_using_the_same_key() throws NoSuchAlgorithmException, BadPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException {
        SymmetricKeysGenerator generator = new SymmetricKeysGenerator("AES", 256);
        SymmetricKey key = generator.getKey();

        String message = "this is a secret message";

        SymmetricEncrypter encrypter = new SymmetricEncrypter("UTF8", "AES", key);
        String crypted = encrypter.encrypt(message);

        SymmetricDecrypter decrypter = new SymmetricDecrypter("AES", key);

        Assertions.assertEquals(message, decrypter.decrypt(crypted));
    }

    @Test
    public void encrypt_a_message_using_a_public_key_and_decrypt_it_using_a_private_key() throws NoSuchAlgorithmException, BadPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException {
        AsymmetricKeysGenerator generator = new AsymmetricKeysGenerator("RSA", "SHA1PRNG", 4096);
        AsymmetricKeysPair pair  = generator.getKeyPair();
        AsymmetricPrivateKey privatekey = pair.getPrivatekey();
        AsymmetricPublicKey publickey = pair.getPublickey();

        String message = "this is a secret message";

        AsymmetricEncrypter encrypter = new AsymmetricEncrypter("UTF8", "RSA/ECB/OAEPWithSHA-1AndMGF1Padding", publickey);
        String crypted = encrypter.encrypt(message);

        AsymmetricDecrypter decrypter = new AsymmetricDecrypter("RSA/ECB/OAEPWithSHA-1AndMGF1Padding", privatekey);
        Assertions.assertEquals(message, decrypter.decrypt(crypted));
    }

    @Test
    public void encrypt_a_message_using_a_public_key_and_decrypt_it_using_a_private_key_mixing_symmetric_and_assymetric_encryption() throws NoSuchAlgorithmException, BadPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException {
        AsymmetricKeysGenerator generator = new AsymmetricKeysGenerator("RSA", "SHA1PRNG", 4096);
        AsymmetricKeysPair pair  = generator.getKeyPair();
        AsymmetricPrivateKey privatekey = pair.getPrivatekey();
        AsymmetricPublicKey publickey = pair.getPublickey();

        String message = "this is a secret message";

        MixedEncrypter encrypter = new MixedEncrypter("UTF8", "RSA/ECB/OAEPWithSHA-1AndMGF1Padding", "AES", 256, publickey);
        String crypted = encrypter.encrypt(message);

        MixedDecrypter decrypter = new MixedDecrypter("RSA/ECB/OAEPWithSHA-1AndMGF1Padding", "AES", privatekey);
        Assertions.assertEquals(message, decrypter.decrypt(crypted));
    }

}

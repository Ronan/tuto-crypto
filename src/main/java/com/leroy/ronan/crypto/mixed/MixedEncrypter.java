package com.leroy.ronan.crypto.mixed;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.leroy.ronan.crypto.symmetric.SymmetricKeysGenerator;
import com.leroy.ronan.crypto.asymmetric.AsymmetricEncrypter;
import com.leroy.ronan.crypto.asymmetric.AsymmetricPublicKey;
import com.leroy.ronan.crypto.symmetric.SymmetricEncrypter;
import com.leroy.ronan.crypto.symmetric.SymmetricKey;

public class MixedEncrypter {

    private AsymmetricPublicKey publicKey;
    private String charset;
    private String transformation;
    private String algorithm;
    private int size;

    public MixedEncrypter(String charset, String transformation, String algorithm, int size, AsymmetricPublicKey publicKey) {
        super();
        this.charset = charset;
        this.transformation = transformation;
        this.algorithm = algorithm;
        this.size = size;
        this.publicKey = publicKey;
    }

    public String encrypt(String message) throws NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException {
        SymmetricKey key = new SymmetricKeysGenerator(algorithm, size).getKey();
        
        SymmetricEncrypter encrypter = new SymmetricEncrypter(charset, algorithm, key);
        String crypted = encrypter.encrypt(message);
        
        AsymmetricEncrypter keyEncrypter = new AsymmetricEncrypter(charset, transformation, publicKey);
        String cryptedKey = keyEncrypter.encrypt(key.toString());
        
        MixedCryptedMessage result = new MixedCryptedMessage(cryptedKey, crypted);
        return result.toString();
    }
}

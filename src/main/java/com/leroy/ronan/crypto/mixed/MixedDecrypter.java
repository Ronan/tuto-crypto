package com.leroy.ronan.crypto.mixed;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.leroy.ronan.crypto.CryptoKey;
import com.leroy.ronan.crypto.asymmetric.AsymmetricDecrypter;
import com.leroy.ronan.crypto.asymmetric.AsymmetricPrivateKey;
import com.leroy.ronan.crypto.symmetric.SymmetricDecrypter;
import com.leroy.ronan.crypto.symmetric.SymmetricKey;

public class MixedDecrypter {

    private String transformation;
    private String algorithm;
    
    private AsymmetricPrivateKey privateKey;

    public MixedDecrypter(String transformation, String algorithm, AsymmetricPrivateKey privateKey) {
        super();
        this.transformation = transformation;
        this.algorithm = algorithm;
        this.privateKey = privateKey;
    }

    public String decrypt(String crypted) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
        MixedCryptedMessage message = new MixedCryptedMessage(crypted);
        
        AsymmetricDecrypter keyDecrypter = new AsymmetricDecrypter(transformation, privateKey);
        String keyStr = keyDecrypter.decrypt(message.getCryptedKey());
        SymmetricKey key = new SymmetricKey(CryptoKey.fromString(keyStr, algorithm));

        SymmetricDecrypter msgDecrypter = new SymmetricDecrypter(algorithm, key);
        return msgDecrypter.decrypt(message.getCryptedMsg());
    }
}

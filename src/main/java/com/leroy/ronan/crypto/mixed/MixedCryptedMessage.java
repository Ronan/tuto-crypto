package com.leroy.ronan.crypto.mixed;

public class MixedCryptedMessage {

    private String cryptedKey;
    private String cryptedMsg;

    public MixedCryptedMessage(String cryptedKey, String cryptedMsg) {
        this.cryptedKey = cryptedKey;
        this.cryptedMsg = cryptedMsg;
    }
    
    public MixedCryptedMessage(String crypted) {
        String[] tab = crypted.split("\\|");
        this.cryptedKey = tab[0];
        this.cryptedMsg = tab[1];
    }

    public String toString() {
        return cryptedKey + "|" + cryptedMsg;
    }

    public String getCryptedKey() {
        return cryptedKey;
    }

    public String getCryptedMsg() {
        return cryptedMsg;
    }
}

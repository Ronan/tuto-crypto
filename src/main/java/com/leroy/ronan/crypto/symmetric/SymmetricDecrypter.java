package com.leroy.ronan.crypto.symmetric;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class SymmetricDecrypter {

    private String algorithm;
    private Key key;

    public SymmetricDecrypter(String algorithm, SymmetricKey key) {
        this.algorithm = algorithm;
        this.key = key.getKey();
    }

    public String decrypt(String message) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, key, cipher.getParameters());
        
        byte[] msgByte = Base64.getDecoder().decode(message);
        byte[] bytes = cipher.doFinal(msgByte);
        
        String res = new String(bytes);
        return res;
    }

}

package com.leroy.ronan.crypto.symmetric;

import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;

public class SymmetricKeysGenerator {

    private KeyGenerator keyGen;
    
    public SymmetricKeysGenerator(String algorithm, int size) throws NoSuchAlgorithmException {
        keyGen = KeyGenerator.getInstance(algorithm);
        keyGen.init(size);
    }

    public SymmetricKey getKey() {
        return new SymmetricKey(keyGen.generateKey());
    }

}

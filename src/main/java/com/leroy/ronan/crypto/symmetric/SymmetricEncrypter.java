package com.leroy.ronan.crypto.symmetric;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class SymmetricEncrypter {

    private String charset;
    private String algorithm;
    private Key key;
    
    public SymmetricEncrypter(String charset, String algorithm, SymmetricKey key) {
        this.charset = charset;
        this.algorithm = algorithm;
        this.key = key.getKey();
    }
    
    public String encrypt(String message) throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, key, cipher.getParameters());
        
        byte[] msgByte = message.getBytes(charset);
        byte[] bytes = cipher.doFinal(msgByte);
        
        String res = Base64.getEncoder().encodeToString(bytes);
        return res;
    }

}

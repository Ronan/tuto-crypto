package com.leroy.ronan.crypto;

import java.security.Key;
import java.util.Base64;

import javax.crypto.spec.SecretKeySpec;

public class CryptoKey {

    private Key key;
    
    public CryptoKey(Key key) {
        this.key = key;
    }

    public Key getKey() {
        return key;
    }
    
    public String toString() {
        return Base64.getEncoder().encodeToString(key.getEncoded());
    }
    
    public static Key fromString(String keyStr, String algo) {
        return new SecretKeySpec(Base64.getDecoder().decode(keyStr), algo);
    }

}

package com.leroy.ronan.crypto.asymmetric;

import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class AsymmetricKeysGenerator {

    private KeyPairGenerator keyGen;
    
    public AsymmetricKeysGenerator(String algorithm, String randomAlgorithm, int size) throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance(randomAlgorithm);
        this.keyGen = KeyPairGenerator.getInstance(algorithm);
        this.keyGen.initialize(size, random);
    }

    public AsymmetricKeysPair getKeyPair() {
        return new AsymmetricKeysPair(keyGen.generateKeyPair());
    }

}

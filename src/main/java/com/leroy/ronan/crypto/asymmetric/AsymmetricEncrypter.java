package com.leroy.ronan.crypto.asymmetric;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class AsymmetricEncrypter extends AsymmetricCipher {

    private String charset;
    private Key key;

    public AsymmetricEncrypter(String charset, String transformation, AsymmetricPublicKey publicKey) {
        super(transformation);
        this.charset = charset;
        this.key = publicKey.getKey();
    }

    public String encrypt(String message) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
        byte[] data = message.getBytes(charset);
        byte[] bytes = process(data, key, Cipher.ENCRYPT_MODE);
        return Base64.getEncoder().encodeToString(bytes);
    }

}

package com.leroy.ronan.crypto.asymmetric;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class AsymmetricDecrypter extends AsymmetricCipher {

    private Key key;

    public AsymmetricDecrypter(String transformation, AsymmetricPrivateKey privateKey) {
        super(transformation);
        this.key = privateKey.getKey();
    }

    public String decrypt(String message) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
        byte[] data = Base64.getDecoder().decode(message);
        byte[] bytes = process(data, key, Cipher.DECRYPT_MODE);
        return new String(bytes);
    }

}

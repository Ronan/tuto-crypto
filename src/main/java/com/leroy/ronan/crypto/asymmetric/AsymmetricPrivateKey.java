package com.leroy.ronan.crypto.asymmetric;

import java.security.Key;

import com.leroy.ronan.crypto.CryptoKey;

public class AsymmetricPrivateKey extends CryptoKey {

    public AsymmetricPrivateKey(Key key) {
        super(key);
    }
}

package com.leroy.ronan.crypto.asymmetric;

import java.security.Key;

import com.leroy.ronan.crypto.CryptoKey;

public class AsymmetricPublicKey extends CryptoKey {

    public AsymmetricPublicKey(Key key) {
        super(key);
    }
}

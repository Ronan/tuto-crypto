package com.leroy.ronan.crypto.asymmetric;

import java.security.KeyPair;

public class AsymmetricKeysPair {

    private AsymmetricPrivateKey privateKey;
    private AsymmetricPublicKey publicKey;
    
    public AsymmetricKeysPair(KeyPair key) {
        this.privateKey = new AsymmetricPrivateKey(key.getPrivate());
        this.publicKey = new AsymmetricPublicKey(key.getPublic());
    }

    public AsymmetricPrivateKey getPrivatekey() {
        return privateKey;
    }

    public AsymmetricPublicKey getPublickey() {
        return publicKey;
    }

}
